﻿package com.hf
{
	
	public class Singleton
	{
			
		//--------------------------------------
		// CLASS CONSTANTS
		//--------------------------------------

		private static var _instance:Singleton=null;
		
		private var stageW:Number;
		private var stageH:Number;	
		private var leftPadding:uint = 15;	
		private var sound_mgr:Object;
		private var shell_ref:Object;
		private var collapsed_ref:Object;
		private var expanded_ref:Object;
		private var swfAdd_ref:Object;
		private var splash_ref:Object;
		private var experience_ref:Object;
		private var soundManager:Object;
		private var sec_ref:Object;
		private var assetPath:String;
		private var _totalVideos:Number;
		private var _totalMessages:Number;
		private var _totalPledges:Number;
		private var splashXML:XML;
		private var experienceXML:XML;
		private var experienceSkin:String;
		private var experienceSkinFrame:Number;
		private var refs:Array;
		private var experiencePage:String;
		private var subPage:String;
		private var thumbPath:String;
		private var tracking_ref:Object;
		private var isInitPlay:Boolean = false;

		//--------------------------------------
		//  CONSTRUCTOR
		//--------------------------------------
	
		public function Singleton(e:SingletonEnforcer)
		{
			super();
		}

		//--------------------------------------
		//  PRIVATE VARIABLES
		//--------------------------------------
	
		//--------------------------------------
		//  GETTER/SETTERS
		//--------------------------------------		
		
		public static function getInstance():Singleton{
            if(_instance==null){
                _instance=new Singleton(new SingletonEnforcer());
            }
            return _instance;
        }
		
		public function setShellRef($obj:Object):void {	shell_ref = $obj; }
		public function getShellRef():Object {	return shell_ref; }
		
		public function setCollapsedRef($obj:Object):void {	collapsed_ref = $obj; }
		public function getCollapsedRef():Object {	return collapsed_ref; }
		
		public function setExpandedRef($obj:Object):void {	expanded_ref = $obj; }
		public function getExpandedRef():Object {	return expanded_ref; }
		
		public function setActiveSection($obj:Object):void { sec_ref = $obj; }
		public function getActiveSection():Object {	return sec_ref; }
		
		public function setStageWidth($num:Number):void	{ stageW = $num; };
		public function getStageWidth():Number { return stageW };
		
		public function setStageHeight($num:Number):void { stageH = $num; };
		public function getStageHeight():Number { return stageH };
		
		public function setAssetPath($s:String):void { assetPath = $s; };
		public function getAssetPath():String { return assetPath; };
		
		public function setVideoTotal($t:Number):void { _totalVideos = $t; };
		public function getVideoTotal():Number { return _totalVideos; };
		
		public function setActiveThumbPath($s:String):void { thumbPath = $s; };
		public function getActiveThumbPath():String { return thumbPath; };
		
		public function setSoundManager($obj:Object):void { soundManager = $obj; };
		public function getSoundManager():Object { return soundManager; };
		
		public function setTrackingRef($obj:Object):void { tracking_ref = $obj; };
		public function getTrackingRef():Object { return tracking_ref; };
		
		public function setSubPage($s:String):void { subPage = $s; };
		public function getSubPage():String { return subPage; };
		
		public function setInitPlay($b:Boolean):void { isInitPlay = $b; };
		public function getInitPlay():Boolean { return isInitPlay; };
		
		//--------------------------------------
		//  PUBLIC METHODS
		//--------------------------------------
		
		public function loadSection():void
		{
			shell_ref.loadSection();
		}
		
		public function setSWFAddPage($main:Number, $sub:Number=NaN):void
		{
			//swfAdd_ref.setSubPage($num);
			swfAdd_ref.setPage($main, $sub);
		}
	
	
		//--------------------------------------
		//  EVENT HANDLERS
		//--------------------------------------
	
		//--------------------------------------
		//  PRIVATE & PROTECTED INSTANCE METHODS
		//--------------------------------------
	
	}

}

class SingletonEnforcer{}


﻿package com.hf
{

	public class Constants 
	{
			
		//--------------------------------------
		// CLASS CONSTANTS
		//--------------------------------------
		
		
		public static const STAGE_WIDTH:uint = 300;
		public static const STAGE_HEIGHT:uint = 250;
		public static const ROLLOVER_HEX:String = "0xCCCCCC";
		public static const BLUE_HEX:String = "0x8fa7b5";
		public static const GRAY_HEX:String = "0x333333";
		public static const ORANGE_HEX:String = "0x8fa7b5";
		public static const BROWN_HEX:String = "0xf7bf4c";
		public static const VIDEO_WIDTH:uint = 300;
		public static const VIDEO_HEIGHT:uint = 170;
		public static const BORDER_COLOR:uint = 0xbdbdbd;
	
		//public static const VIDEO_PATH:String = "video/";
		//public static const VIDEO_NAME_INIT:String = "chrisley_300x198_15.flv";

		//public static const VIDEO_NAME:String = "chrisley_300x198_30.flv";
		
		//public static const VIDEO_1:String = 'http://heavyform.com/projects/glow/chrisley/banners/728x90_exp/video/meet_todd_728x90_MAR11.flv';
		/*public static const VIDEO_2:String = 'http://heavyform.com/projects/glow/chrisley/banners/728x90_exp/video/meet_todd_728x90_TOM.flv';
		public static const VIDEO_3:String = 'http://heavyform.com/projects/glow/chrisley/banners/728x90_exp/video/meet_todd_728x90_TON.flv';
		public static const VIDEO_4:String = 'http://heavyform.com/projects/glow/chrisley/banners/728x90_exp/video/meet_todd_728x90_TUE.flv';
		public static const VIDEO_5:String = 'http://heavyform.com/projects/glow/chrisley/banners/728x90_exp/video/KIDS_728x90_MAR11.flv';
		public static const VIDEO_6:String = 'http://heavyform.com/projects/glow/chrisley/banners/728x90_exp/video/KIDS_728x90_TOM.flv';
		public static const VIDEO_7:String = 'http://heavyform.com/projects/glow/chrisley/banners/728x90_exp/video/KIDS_728x90_TON.flv';
		public static const VIDEO_8:String = 'http://heavyform.com/projects/glow/chrisley/banners/728x90_exp/video/KIDS_728x90_TUE.flv';
		public static const VIDEO_9:String = 'http://heavyform.com/projects/glow/chrisley/banners/728x90_exp/video/CRAZY_728x90_MAR11.flv';
		public static const VIDEO_10:String = 'http://heavyform.com/projects/glow/chrisley/banners/728x90_exp/video/CRAZY_728x90_TOM.flv';
		public static const VIDEO_11:String = 'http://heavyform.com/projects/glow/chrisley/banners/728x90_exp/video/CRAZY_728x90_TON.flv';
		public static const VIDEO_12:String = 'http://heavyform.com/projects/glow/chrisley/banners/728x90_exp/video/CRAZY_728x90_TUE.flv';*/
		
		public static const LIVE:Boolean = true;
		
		public static const MAIN_CLICK_TAG:String = "clicktag1";
		 //public static const FACEBOOK_CLICK_TAG:String = "clicktag2"; 
		 //public static const TWITTER_CLICK_TAG:String = "clicktag3";
		 //public static const ITUNES_CLICK_TAG:String = "clicktag4"; 
		 //public static const TICKETS_CLICK_TAG:String = "clicktag5"; 
		 //public static const TRAILER_CLICK_TAG:String = "clicktag2";
	
		public static const PAGE_FADE_OUT_SPEED:Number = 1;
		
		public static const BG_ALPHA_FADE_VALUE:Number = .45;
		
		public static const IS_RATING_VISIBLE:Boolean = false;

		//--------------------------------------
		//  CONSTRUCTOR
		//--------------------------------------

		//--------------------------------------
		//  PRIVATE VARIABLES
		//--------------------------------------
	
		//--------------------------------------
		//  GETTER/SETTERS
		//--------------------------------------
	
		//--------------------------------------
		//  PUBLIC METHODS
		//--------------------------------------


		//--------------------------------------
		//  EVENT HANDLERS
		//--------------------------------------
	
		//--------------------------------------
		//  PRIVATE & PROTECTED INSTANCE METHODS
		//--------------------------------------
	
	}

}


﻿package com.hf
{
	import flash.display.MovieClip;
	import flash.display.MovieClip;
	import flash.events.Event;
	import flash.net.*;
	import flash.external.ExternalInterface;
	
	import com.greensock.*;
	import com.greensock.easing.*;
	import com.greensock.plugins.*;
	TweenPlugin.activate([BlurFilterPlugin]);
	TweenPlugin.activate([TintPlugin]);
	TweenPlugin.activate([ColorTransformPlugin]);
	import flash.events.MouseEvent;
	import flash.display.SimpleButton;
	import flash.utils.*;
	
	public class psych_300x250 extends MovieClip {
		
		//mc instances on stage
		public var hello_mc:MovieClip;
		public var colors_mc:MovieClip;
		public var black_mc:MovieClip;
		public var cover_mc:MovieClip;
		public var progress_mc:MovieClip;
		
		public var dc: DateCheck;
		//replay button added to increase usability
		public var replay_btn:SimpleButton;
		
		//color values
		public var black:Number = 0x222222;
		public var blue:Number = 0x233A5C;
		public var white:Number = 0xEDEEE9;
		public var red:Number = 0x9B1C27;
		
		// TimelineLite reference
		public var tl:TimelineLite;
		
		public function psych_300x250() {
			addEventListener(Event.ADDED_TO_STAGE, init);
			
		}
		private function getClickTag():String {
			return root.loaderInfo.parameters.clickTag;
			/*
		   for (var key:String in root.loaderInfo.parameters) {
			//if (key.substr(0, 8).toLowerCase() == "clicktag")
			trace(root.loaderInfo.parameters);
			trace('key:' + key);
			if (key.toLowerCase() == "clicktag") {
			 return root.loaderInfo.parameters[key];
			}
		   }
		   return "";*/
		  }
		  
		  private function getBrowserName():String
		  {
		   var browser:String;
		   //Uses external interface to reach out to browser and grab browser useragent info.
		   var browserAgent:String = ExternalInterface.call("function getBrowser(){return navigator.userAgent;}");
		   //Determines brand of browser using a find index. If not found indexOf returns (-1).
		   if(browserAgent != null && browserAgent.indexOf("Firefox")>= 0) {
			browser = "Firefox";
		   }
		   else if(browserAgent != null && browserAgent.indexOf("Safari")>= 0){
			browser = "Safari";
		   }
		   else if(browserAgent != null && browserAgent.indexOf("MSIE")>= 0){
			browser = "IE";
		   }
		   else if(browserAgent != null && browserAgent.indexOf("Opera")>= 0){
			browser = "Opera";
		   }
		   else {
			browser = "Undefined";
		   }
		   return (browser);
		  }
		
		
		  private function openWindow(url:String, target:String = '_blank', features:String=""):void
		  {
		   var WINDOW_OPEN_FUNCTION:String = "window.open";
		   var myURL:URLRequest = new URLRequest(url);
		   var browserName:String = getBrowserName();
		   switch (browserName)
		   {
			//If browser is Firefox, use ExternalInterface to call out to browser
			//and launch window via browser's window.open method.
			case "Firefox":
			 ExternalInterface.call(WINDOW_OPEN_FUNCTION, url, target, features);
			   break;
			//If IE,
			case "IE":
			 ExternalInterface.call("function setWMWindow() {window.open('" + url + "', '"+target+"', '"+features+"');}");
			 break;
			// If Safari or Opera or any other
			case "Safari":
			 navigateToURL(myURL, target);
			 break;
			case "Opera":
			 navigateToURL(myURL, target);
			 break;
			default:
			 navigateToURL(myURL, target);
			 break;
		   }
		  }
		  
		  private function ClickTag(e:Event):void
		  {
		   var sURL:String = getClickTag();
		   trace('clicktag url:' + sURL);
		   openWindow(sURL);
		  }
		  
	
		  function showPineaple(e:MouseEvent) {
			    pineapple.visible = true;
				TweenLite.to (pineapple, 0, {alpha:1, ease:Quint.easeOut});
				TweenLite.to (overlay, .4, {alpha:1, ease:Quint.easeOut});
				TweenLite.to (pineapple, .4, {y: 109, ease:Quint.easeOut});
				TweenLite.to (pineapple, .05, {rotation: 3, ease:Quint.easeOut, delay:.3});
				TweenLite.to (pineapple, .05, {rotation: -3, ease:Quint.easeOut, delay:.35});
				TweenLite.to (pineapple, .05, {rotation: 3, ease:Quint.easeOut, delay:.4});
				TweenLite.to (pineapple, .05, {rotation: -3, ease:Quint.easeOut, delay:.45});
				TweenLite.to (pineapple, .05, {rotation: 3, ease:Quint.easeOut, delay:.5});
				TweenLite.to (pineapple, .05, {rotation: -2, ease:Quint.easeOut, delay:.55});
				TweenLite.to (pineapple, .05, {rotation: 2, ease:Quint.easeOut, delay:.6});
				TweenLite.to (pineapple, .05, {rotation: -2, ease:Quint.easeOut, delay:.65});
				TweenLite.to (pineapple, .05, {rotation: 1, ease:Quint.easeOut, delay:.7});
				TweenLite.to (pineapple, .05, {rotation: -1, ease:Quint.easeOut, delay:.75});
				TweenLite.to (pineapple, .05, {rotation: 0, ease:Quint.easeOut, delay:.8});
			}
			
			private function hidePineaple(e:MouseEvent) {
				TweenLite.to (overlay, .3, {alpha:0, ease:Quint.easeOut});
				TweenLite.to (pineapple, .3, {y: 206, ease:Quint.easeOut});
				TweenLite.to (pineapple, 0, {alpha:0, ease:Quint.easeOut, delay:.3});
			}
			
			private function my_delayedFunction (arg1) {
				 TT.visible = true;
				 TT.alpha = 100;
			}
		  
		private function init(e:Event):void {
			trace('init');
			btn.buttonMode = true;
			btn.useHandCursor = true;
			removeEventListener(Event.ADDED_TO_STAGE, init);
			btn.addEventListener(MouseEvent.CLICK, ClickTag); 
			TT.addEventListener(MouseEvent.CLICK, ClickTag); 
//			replay_btn.addEventListener(MouseEvent.CLICK, replay);
			TT.addEventListener(MouseEvent.MOUSE_OVER, showPineaple);
			TT.addEventListener(MouseEvent.MOUSE_OUT, hidePineaple); 
			TT.buttonMode = true;
			TT.visible = false;
	
			usa_logo1.visible = false;
			usa_logo2.visible = false;
			usa_logo3.visible = false;
			usa_logo4.visible = false;
			
			pineapple.visible = false;

			////////DATE CHECK
			dc = new DateCheck();
			dateTxt.gotoAndStop(dc.getDate());
			dateTxt.mouseChildren = false;
			dateTxt.mouseEnabled = false;
			
			dc.getDate();
			trace("num="+dc.getDate());
			
			switch(dc.getDate()) {
				case 1 : 
				case 2 :	
					//vp = new VideoPlayer('video/nurse_300x198_15_generic.flv',true);
					dateTxt.gotoAndStop(1);
					usa_logo1.visible = true;
					//clickBtn.video_dateTxt.gotoAndStop(1);

					break;
				case 3 : 
					//vp = new VideoPlayer('video/nurse_300x198_15_intheaters.flv',true);
					dateTxt.gotoAndStop(2);
					usa_logo2.visible = true;
					//clickBtn.video_dateTxt.gotoAndStop(2);

					break;
				case 4 : 
					//vp = new VideoPlayer('video/nurse_300x198_15_ondemand.flv',true);
					dateTxt.gotoAndStop(3);
					usa_logo3.visible = true;
					//clickBtn.video_dateTxt.gotoAndStop(3);

					break;
				case 5 :
					//vp = new VideoPlayer('video/nurse_300x198_15_ondemand.flv',true);
					dateTxt.gotoAndStop(4);
					usa_logo4.visible = true;
					//clickBtn.video_dateTxt.gotoAndStop(4);

					break;
				default : 
					//vp = new VideoPlayer('video/nurse_300x198_15_generic.flv',true);
					dateTxt.gotoAndStop(1);
					usa_logo1.visible = true;
					//clickBtn.video_dateTxt.gotoAndStop(1);
					break;
			}
			
			
			//constructor
			tl = new TimelineLite();
			
			//tweens
			
			TweenLite.to (box, .1, {alpha:0});
			TweenLite.to (intro, 0, {alpha:1});
			TweenLite.to (intro.suck, 1.5, {alpha:1,  blurFilter:{blurX:0, blurY:0},ease:Quint.easeIn});
			//TweenLite.from (intro, .6, {y:-100, blurFilter:{blurY:32},delay:.1,ease:Quint.easeIn});
			//tl.insert(TweenLite.to (intro.mask_intro, 0.2, {scaleY:3, ease:Linear.easeNone, delay:.9}));
			tl.insert(TweenLite.to (intro.very_end, 0.5, {y:-1.1, blurFilter:{blurY:0}, ease:Quint.easeOut, delay:2.75}));
			//tl.insert(TweenLite.to (intro, 2, {x: 90.6, y:80, scaleX:.65, scaleY:.65,ease:Quint.easeIn, delay:1.6}));
			tl.insert(TweenLite.to (intro, 3.4, {scaleX:.1, scaleY:.1, x:155, y:27.6, ease:Quad.easeIn, delay:2.75}));
			
			tl.insert(TweenLite.to (intro, .8, {alpha:0,ease:Quint.easeOut, delay:5.55}));
			
			
		
			

			tl.insert( TweenLite.to (greenbox, .8, {alpha:0, ease:Quint.easeOut, delay:6.05}))
			tl.insert( TweenLite.from (shawn, 2, {x:-266, y:80, scaleX:2.25, scaleY:2.25, tint:0x050505, tintAmount:0.5, ease:Linear.easeNone, delay:4.05}));
			tl.insert( TweenLite.from (gus, 2, {x:160, y:127, scaleX:2.25, scaleY:2.25, colorTransform:{redOffset : 5, greenOffset : 5, blueOffset : 5}, ease:Linear.easeNone, delay:4.05}));
			tl.insert( TweenLite.to (fade, .6, {alpha:1,  ease:Quint.easeOut, delay:6.05}));
			tl.insert( TweenLite.to (shawn, .4, {x:23, y:22, scaleX:1, scaleY:1, tint:null, tintAmount:0, ease:Linear.easeNone, delay:6.05}));
			tl.insert( TweenLite.to (gus, .4, {x:133, y:29, scaleX:1, scaleY:1, tint:null, tintAmount:0, ease:Linear.easeNone, delay:6.05}));
			
			
			
			
			
			setTimeout(my_delayedFunction, 4000, "");

			
			tl.insert( TweenLite.from (TT, .4, {x: 54, y: 270, ease:Cubic.easeIn, delay:5.95}));
			tl.insert( TweenLite.to (TT, .4, {x: 54, y: 136, ease:Quint.easeOut, delay:6.35}));
			tl.insert( TweenLite.to (tagline.tag_right, .4, {x: 71,  ease:Quint.easeOut, delay:6.35}));
			tl.insert( TweenLite.to (tagline.tag_left, .4, {x: 0,  ease:Quint.easeOut, delay: 6.35}));
			tl.insert( TweenLite.from (usa_logo1.usa_u, .5, {x: 18, alpha:0,ease:Quint.easeOut, delay:6.35}));
			tl.insert( TweenLite.from (usa_logo1.usa_a, .5, {x: 0, alpha:0,ease:Quint.easeOut, delay:6.35}));
			tl.insert( TweenLite.from (usa_logo2.usa_u, .5, {x: 18, alpha:0,ease:Quint.easeOut, delay:6.35}));
			tl.insert( TweenLite.from (usa_logo2.usa_a, .5, {x: 0, alpha:0,ease:Quint.easeOut, delay:6.35}));
			tl.insert( TweenLite.from (usa_logo3.usa_u, .5, {x: 18, alpha:0,ease:Quint.easeOut, delay:6.35}));
			tl.insert( TweenLite.from (usa_logo3.usa_a, .5, {x: 0, alpha:0,ease:Quint.easeOut, delay:6.35}));
			tl.insert( TweenLite.from (usa_logo4.usa_u, .5, {x: 18, alpha:0,ease:Quint.easeOut, delay:6.35}));
			tl.insert( TweenLite.from (usa_logo4.usa_a, .5, {x: 0, alpha:0,ease:Quint.easeOut, delay:6.35}));
			
			tl.insert( TweenLite.from (dateTxt, .4, {x:-155, blurFilter:{blurX:60},ease:Quint.easeOut, delay:6.15}));
			tl.insert( TweenLite.from (message_txt, .6, {x:-252, blurFilter:{blurX:60},ease:Quint.easeOut, delay:6.15}));
			//tl.insert( TweenLite.to (message_txt, .6, {x:70, blurFilter:{blurX:0},delay:5.3}));


			
			
		
			//trace("duration  = " + tl.duration);			
			
		}
		
		
		//event handlers
		
		//adjust the progress bar
		/*private function updateHandler():void{
			progress_mc.scaleX = tl.currentProgress;
		}*/
				
		
	}
	
}
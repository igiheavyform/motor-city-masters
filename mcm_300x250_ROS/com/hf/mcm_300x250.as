﻿package com.hf
{
	import flash.display.MovieClip;
	import flash.display.MovieClip;
	import flash.events.Event;
	import flash.net.*;
	import flash.external.ExternalInterface;
	
	import com.greensock.*;
	import com.greensock.easing.*;
	import com.greensock.plugins.*;
	import flash.geom.*;
	import com.greensock.motionPaths.*;
	TweenPlugin.activate([BlurFilterPlugin]);
	//TweenPlugin.activate([TintPlugin]);
	//TweenPlugin.activate([ColorTransformPlugin]);
	import flash.events.MouseEvent;
	import flash.display.SimpleButton;
	import flash.utils.*;
	import flash.geom.Point;
	
	public class mcm_300x250 extends MovieClip {
		
		//mc instances on stage
		public var hello_mc:MovieClip;
		public var colors_mc:MovieClip;
		public var black_mc:MovieClip;
		public var cover_mc:MovieClip;
		public var progress_mc:MovieClip;
		
		public var dc: DateCheck;
		//replay button added to increase usability
		public var replay_btn:SimpleButton;
		
		//color values
		public var black:Number = 0x222222;
		public var blue:Number = 0x233A5C;
		public var white:Number = 0xEDEEE9;
		public var red:Number = 0x9B1C27;
		
		// TimelineLite reference
		public var tl:TimelineLite;
		
		public function mcm_300x250() {
			addEventListener(Event.ADDED_TO_STAGE, init);
			
		}
		private function getClickTag():String {
			//return root.loaderInfo.parameters.clickTag;
			
		   for (var key:String in root.loaderInfo.parameters) {
			//if (key.substr(0, 8).toLowerCase() == "clicktag")
			trace(root.loaderInfo.parameters);
			trace('key:' + key);
			if (key.toLowerCase() == "clicktag1") {
			 return root.loaderInfo.parameters[key];
			}
		   }
		   return "";
		  }
		  
		private function getClickTag2():String {
			for (var key:String in root.loaderInfo.parameters) {
				//if (key.substr(0, 8).toLowerCase() == "clicktag")
				trace(root.loaderInfo.parameters);
				trace('key:' + key);
				if (key.toLowerCase() == "clicktag2") {
					return root.loaderInfo.parameters[key];
				}
			}
			return "";
		}
		
		private function getClickTag3():String {
			for (var key:String in root.loaderInfo.parameters) {
				//if (key.substr(0, 8).toLowerCase() == "clicktag")
				trace(root.loaderInfo.parameters);
				trace('key:' + key);
				if (key.toLowerCase() == "clicktag3") {
					return root.loaderInfo.parameters[key];
				}
			}
			return "";
		}
		  
		  private function getBrowserName():String
		  {
		   var browser:String;
		   //Uses external interface to reach out to browser and grab browser useragent info.
		   var browserAgent:String = ExternalInterface.call("function getBrowser(){return navigator.userAgent;}");
		   //Determines brand of browser using a find index. If not found indexOf returns (-1).
		   if(browserAgent != null && browserAgent.indexOf("Firefox")>= 0) {
			browser = "Firefox";
		   }
		   else if(browserAgent != null && browserAgent.indexOf("Safari")>= 0){
			browser = "Safari";
		   }
		   else if(browserAgent != null && browserAgent.indexOf("MSIE")>= 0){
			browser = "IE";
		   }
		   else if(browserAgent != null && browserAgent.indexOf("Opera")>= 0){
			browser = "Opera";
		   }
		   else {
			browser = "Undefined";
		   }
		   return (browser);
		  }
		
		
		  private function openWindow(url:String, target:String = '_blank', features:String=""):void
		  {
		   var WINDOW_OPEN_FUNCTION:String = "window.open";
		   var myURL:URLRequest = new URLRequest(url);
		   var browserName:String = getBrowserName();
		   switch (browserName)
		   {
			//If browser is Firefox, use ExternalInterface to call out to browser
			//and launch window via browser's window.open method.
			case "Firefox":
			 ExternalInterface.call(WINDOW_OPEN_FUNCTION, url, target, features);
			   break;
			//If IE,
			case "IE":
			 ExternalInterface.call("function setWMWindow() {window.open('" + url + "', '"+target+"', '"+features+"');}");
			 break;
			// If Safari or Opera or any other
			case "Safari":
			 navigateToURL(myURL, target);
			 break;
			case "Opera":
			 navigateToURL(myURL, target);
			 break;
			default:
			 navigateToURL(myURL, target);
			 break;
		   }
		  }
		  
		  private function ClickTag(e:Event):void  {
		   var sURL:String = getClickTag();
		   trace('clicktag url:' + sURL);
		   openWindow(sURL);
		  }
		  
		  private function ClickTag2(e:Event):void {
		   var sURL:String = getClickTag2();
		   trace('clicktag url2:' + sURL);
		   openWindow(sURL);
		  }
		  
		  private function ClickTag3(e:Event):void {
		   var sURL:String = getClickTag3();
		   trace('clicktag url3:' + sURL);
		   openWindow(sURL);
		  }
	

			
			/*private function my_delayedFunction (arg1) {
				 TT.visible = true;
				 TT.alpha = 100;
			}*/
		  
		private function init(e:Event):void {
			trace('init');
			btn.buttonMode = true;
			btn.useHandCursor = true;
			removeEventListener(Event.ADDED_TO_STAGE, init);
			btn.addEventListener(MouseEvent.CLICK, ClickTag); 
			//TT.addEventListener(MouseEvent.CLICK, ClickTag); 
//			replay_btn.addEventListener(MouseEvent.CLICK, replay);
			//TT.buttonMode = true;
			//TT.visible = false;
			fb_ico.buttonMode = true;
			fb_ico.addEventListener(MouseEvent.CLICK, ClickTag2); 
			tw_ico.buttonMode = true;
			tw_ico.addEventListener(MouseEvent.CLICK, ClickTag3);

			////////DATE CHECK
			dc = new DateCheck();
			dateTxt.gotoAndStop(dc.getDate());
			dateTxt.mouseChildren = false;
			dateTxt.mouseEnabled = false;
			
			dc.getDate();
			trace("num="+dc.getDate());
			
			switch(dc.getDate()) {
				case 1 : 
				case 2 :	
					//vp = new VideoPlayer('video/nurse_300x198_15_generic.flv',true);
					dateTxt.gotoAndStop(1);
					//clickBtn.video_dateTxt.gotoAndStop(1);

					break;
				case 3 : 
					//vp = new VideoPlayer('video/nurse_300x198_15_intheaters.flv',true);
					dateTxt.gotoAndStop(2);
					//clickBtn.video_dateTxt.gotoAndStop(2);

					break;
				case 4 : 
					//vp = new VideoPlayer('video/nurse_300x198_15_ondemand.flv',true);
					dateTxt.gotoAndStop(3);
					//clickBtn.video_dateTxt.gotoAndStop(3);

					break;
				case 5 :
					//vp = new VideoPlayer('video/nurse_300x198_15_ondemand.flv',true);
					dateTxt.gotoAndStop(4);
					//clickBtn.video_dateTxt.gotoAndStop(4);

					break;
				default : 
					//vp = new VideoPlayer('video/nurse_300x198_15_generic.flv',true);
					dateTxt.gotoAndStop(1);
					//clickBtn.video_dateTxt.gotoAndStop(1);
					break;
			}
			
			// time offset
			var d = .4;
			var g = .6;
			//TT.cacheAsBitmap = true;
			//TTmask.cacheAsBitmap = true;
			//TT.mask = TTmask;
			
			//constructor
			tl = new TimelineLite();
			
			//tweens
			
			tl.insert( TweenLite.to (box, 2, {alpha:0, ease:Cubic.easeOut}));
			tl.insert( TweenLite.to (intro, 6, {x:-8,ease:Quint.easeOut}));
			tl.insert( TweenLite.to (intro, .8, {alpha:1,ease:Quint.easeOut, delay:.2}));
			tl.insert( TweenLite.from (tagline, 6, {x: 100, ease:Cubic.easeOut, delay:1.8}));
			tl.insert( TweenLite.to (tagline, 3, {alpha:1, ease:Quint.easeOut, delay:1.8}));
			
			
			tl.insert( TweenLite.from (intro.car_mask.carm001, .6, {x:276 , scaleX:.1 ,ease:Cubic.easeOut, delay:.9}));
			tl.insert( TweenLite.from (intro.car_mask.carm002, .5, {y:275.8 , scaleY:.1 ,ease:Cubic.easeOut, delay:1.2}));
			tl.insert( TweenLite.from (intro.car_mask.carm003, .6, {x: 370, y:148 , ease:Cubic.easeOut, delay:1.1}));
			tl.insert( TweenLite.from (intro.car_mask.carm004, .6, {x: 355, y:72 , ease:Cubic.easeOut, delay:1.3}));
			tl.insert( TweenLite.from (intro.car_mask.carm005, .6, {x: 255,scaleX: .1,scaleY:.1, ease:Cubic.easeOut, delay:1.6}));
			//tl.insert( TweenLite.from (intro.car_mask.carmfill, .1, {x:404,y:223, scaleX:.1, scaleY:.1,ease:Cubic.easeOut, delay:1.9}));
			tl.insert( TweenLite.from (intro.car_mask.carm006, 3, {x:440, ease:Cubic.easeOut, delay:1.1}));
			tl.insert( TweenLite.from (intro.car_mask.carm007, .4, {y:255, ease:Cubic.easeOut, delay:1.8}));
			tl.insert( TweenLite.from (intro.car_mask.carm008, .6, {x:153,scaleX:.1, ease:Cubic.easeOut, delay:1.9}));
			tl.insert( TweenLite.from (intro.car_mask.carm009, .7, {x:185,y:16, ease:Cubic.easeOut, delay:2}));
			tl.insert( TweenLite.from (intro.car_mask.carm010, .7, {x:324,y:-50, ease:Cubic.easeOut, delay:2.1}));
			tl.insert( TweenLite.from (intro.car_mask.carm011, .4, {x:339,y:-50, ease:Cubic.easeOut, delay:2.2}));
			tl.insert( TweenLite.from (intro.car_mask.carm012, .4, {x:390,y:-100, ease:Cubic.easeOut, delay:2.2}));
			tl.insert( TweenLite.from (intro.car_mask.carm013, .4, {x:393,y:-160, ease:Cubic.easeOut, delay:2.3}));
			
			tl.insert( TweenLite.to (tagline.mask_txt.mask001, .3, {y:0, scaleY:25, ease:Cubic.easeOut, delay:2.1+d}));
			tl.insert( TweenLite.to (tagline.mask_txt.mask002, .4, {x:62, scaleY:31, ease:Cubic.easeOut, delay:2.2+d}));
			tl.insert( TweenLite.to (tagline.mask_txt.mask003, .3, {y:-1, ease:Cubic.easeOut, delay:2.3+d}));
			tl.insert( TweenLite.to (tagline.mask_txt.mask004, .3, {scaleX:7, ease:Cubic.easeOut, delay:2.3+d}));
			tl.insert( TweenLite.to (tagline.mask_txt.mask005, .4, {y:-4, ease:Cubic.easeOut, delay:2.2+d}));
			tl.insert( TweenLite.to (tagline.mask_txt.mask006, .3, {x:116, scaleX:16.3,ease:Cubic.easeIn, delay:2.3+d}));
			tl.insert( TweenLite.to (tagline.mask_txt.mask007, .3, {y:1,ease:Cubic.easeOut, delay:2.3+d}));
			tl.insert( TweenLite.to (tagline.mask_txt.mask008, .4, {x:208,ease:Cubic.easeOut, delay:2.1+d}));
			tl.insert( TweenLite.from (tagline.mask_txt.mask012, .4, {scaleY:.1,ease:Cubic.easeOut, delay:2.4+d}));
			tl.insert( TweenLite.from (tagline.mask_txt.mask010, .3, {scaleX:.1,ease:Cubic.easeIn, delay:2.5+d}));
			tl.insert( TweenLite.to (tagline.mask_txt.mask009, .3, {y:31,ease:Cubic.easeIn, delay:2.5+d}));
			tl.insert( TweenLite.from (tagline.mask_txt.mask011, .3, {x:64, scaleX:.1,ease:Cubic.easeIn, delay:2.5+d}));
			tl.insert( TweenLite.from (tagline.mask_txt.mask013, .3, {scaleY:.1,ease:Cubic.easeOut, delay:2.3+d}));	
			tl.insert( TweenLite.from (tagline.mask_txt.mask014, .5, {x: 119, scaleX:.1,ease:Cubic.easeOut, delay:2.1+d}));
			tl.insert( TweenLite.from (tagline.mask_txt.mask015, .3, {x: 130, scaleX:.1,ease:Cubic.easeIn, delay:2.5+d}));
			tl.insert( TweenLite.from (tagline.mask_txt.mask016, .4, {y: 29, scaleY:.1,ease:Cubic.easeOut, delay:2.5+d}));
			tl.insert( TweenLite.from (tagline.mask_txt.mask019, .3, {x: 164.70, scaleX:.1,ease:Cubic.easeOut, delay:2.5+d}));
			tl.insert( TweenLite.from (tagline.mask_txt.mask020, .5, {x: 197, scaleX:.1,ease:Cubic.easeOut, delay:2.3+d}));
			tl.insert( TweenLite.to (tagline.mask_txt.mask021, .3, {y:29,ease:Cubic.easeIn, delay:2.7+d}));
			tl.insert( TweenLite.from (tagline.mask_txt.mask023, .3, {x:255,ease:Cubic.easeIn, delay:2.7+d}));
			tl.insert( TweenLite.from (tagline.mask_txt.mask024, .5, {y: 29, scaleY:.1,ease:Cubic.easeIn, delay:2.3+d}));	
			tl.insert( TweenLite.from (tagline.mask_txt.mask025, .3, {y: 89, ease:Cubic.easeIn, delay:2.8+d}));
			tl.insert( TweenLite.from (tagline.mask_txt.mask026, .4, {x: 212, scaleX:.1, ease:Cubic.easeIn, delay:2.7+d}));
			tl.insert( TweenLite.from (tagline.mask_txt.mask027, .6, {y: 58, scaleY:.1, ease:Cubic.easeIn, delay:2.5+d}));
			
			var path:LinePath2D = new LinePath2D([new Point(11, 191),
					   new Point(18, 183),
					   new Point(31, 180),
					   new Point(42, 178),
					   new Point(53, 177),
					   new Point(66, 172),
					   new Point(80,169),
					   new Point(95,168),
					   new Point(111,170),	
					   new Point(125, 173)]);
			 
			 
			// We can add the path
			addChild(path);
			path.alpha = 0;
			 
			// You add the item (in this case, an MC called mySquare) to the Path as a
			// follower.  Counter-intuitive, but the way it works.
			 
			path.addFollower(flare);
			 
			// This is the line that actually starts the tween.  This is a 20-second
			// tween that goes all the way to the end of the path.	
			tl.insert( TweenLite.to(flare, .2, {alpha:1, delay:4.8+g}));
			tl.insert( TweenLite.to(path, .8, {progress:1, ease:Cubic.easeOut, delay: 4.8+g}));
			tl.insert( TweenLite.to(flare, .2, {alpha:0, delay:5.1+g}));
			
			tl.insert( TweenLite.to (flare_BIG, .4, {alpha: 1, ease:Quint.easeOut, delay:4.4+g}));
			//tl.insert( TweenLite.from (TT, .8, {x: -1913, y: 100, scaleX:24, scaleY:24, alpha: 0, blurFilter:{blurX:20, blurY:20}, ease:Cubic.easeIn, delay:4}));
			//tl.insert( TweenLite.from (TTmask, .8, {x: 170, ease:Cubic.easeIn, delay:4.3+g}));
			tl.insert( TweenLite.to (TT, .8, {alpha: 1, ease:Cubic.easeIn, delay:4.3+g}));
			tl.insert( TweenLite.to (flare_BIG, .8, {alpha: 0, ease:Cubic.easeIn, delay:4.8+g}));		
			tl.insert( TweenLite.to (tune_up, 1.8, {alpha:1,ease:Cubic.easeOut, delay:5.0+g}));
			tl.insert( TweenLite.to (dateTxt, 1.8, {alpha:1,ease:Cubic.easeOut, delay:5.5+g}));
			// Proba
			
			tl.insert( TweenLite.to (truTV_logo, 2.2, {alpha:1, ease:Cubic.easeOut, delay:6.3+g}));	
			tl.insert( TweenLite.to (flare2, .4, {alpha:1, delay:7.0+g}));
			tl.insert( TweenLite.from (flare2, .8, {x:242, delay:7.0+g}));
			tl.insert( TweenLite.to (flare2, .4, {alpha:0, delay:7.6+g}));
			
			tl.insert( TweenLite.to (fb_ico, .4, {alpha:1, delay:7.8+g}));
			tl.insert( TweenLite.to (tw_ico, .4, {alpha:1, delay:8+g}));

		
			//trace("duration  = " + tl.duration);			
			
		}
		
		
		//event handlers
		
		//adjust the progress bar
		/*private function updateHandler():void{
			progress_mc.scaleX = tl.currentProgress;
		}*/
				
		
	}
	
}
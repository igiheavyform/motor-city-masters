﻿package com.hf
{

	import flash.events.*;
	import flash.display.*;
	import flash.utils.*;
	import flash.net.NetConnection;
	import flash.net.NetStream;
	import flash.media.*;

	import com.greensock.*;
	import com.greensock.easing.*;

	public class VideoPlayer extends MovieClip
	{
			
		//--------------------------------------
		// CLASS CONSTANTS
		//--------------------------------------
		
		//--------------------------------------
		//  CONSTRUCTOR
		//--------------------------------------
	
		public function VideoPlayer($videoName, $isInit:Boolean = false)
		{
			isInit = $isInit;
			videoName = $videoName;
			addEventListener(Event.ADDED_TO_STAGE, init);
			addEventListener(Event.REMOVED_FROM_STAGE, kill);
		}

		//--------------------------------------
		//  PRIVATE VARIABLES
		//--------------------------------------
		
		private var _videofile:String;
		private var _initVolume:Number = 1;
		private var sndTransform:SoundTransform;
		private var sndControl:VolumeMC;
		private var ns:NetStream;
		private var nc:NetConnection;
		private var vid:Video;
		private var isInit:Boolean;
		private var videoName:String;

		//--------------------------------------
		//  GETTER/SETTERS
		//--------------------------------------
	
		public function set initVolume($n:Number):void
		{
			_initVolume = $n;
		}

		public function get initVolume():Number
		{
			return _initVolume;
		}

		//--------------------------------------
		//  PUBLIC METHODS
		//--------------------------------------
	
		public function toggleMute($s:String):void
		{
			switch($s) {
				case "ON":
				    _initVolume = 1;
				    //sndControl.visible	= true;
					break;
				case "OFF":
					_initVolume = 0;
				    //sndControl.visible	= true;
					break;

			}

			sndTransform = new SoundTransform(_initVolume);
		    ns.soundTransform	= sndTransform;
		}

		public function stopVid():void
		{
			ns.close();
		}

		/*public function swapVid($v:String):void
		{
			videoName = $v;
			ns.play(Constants.VIDEO_PATH + videoName);
		}*/

		public function replayVid():void
		{
			ns.seek(0);
		}

		//--------------------------------------
		//  EVENT HANDLERS
		//--------------------------------------

		private function init(e:Event):void
		{

			vid = new Video(uint(Constants.VIDEO_WIDTH), uint(Constants.VIDEO_HEIGHT));
			addChild(vid);

			nc = new NetConnection();
			nc.connect(null);

			ns = new NetStream(nc);
			vid.attachNetStream(ns);

			var listener:Object = new Object();
			listener.onMetaData = function(evt:Object):void {};
			ns.client = listener;

			//ns.play(Constants.VIDEO_PATH + Constants.VIDEO_NAME);
			//ns.play(Constants.VIDEO_PATH + videoName);

			ns.addEventListener(NetStatusEvent.NET_STATUS, statusChanged);

		   /* sndControl = new VolumeMC();
			sndControl.x = 265;
		    sndControl.y = 180;
		    //trace(this.parent.parent.soundHolder.x);
			addChild(sndControl);
		    sndControl.addEventListener("BUTTON_CLICKED", muteClicked);
	*/
		    if(isInit) {
		    	//sndControl.setFrame(2);
				_initVolume = 0;
			}

		    sndTransform = new SoundTransform(_initVolume);
		    ns.soundTransform	= sndTransform;

		}
		
		private function kill(e:Event):void
		{
			removeEventListener(Event.REMOVED_FROM_STAGE, kill);
		}

		private function statusChanged(stats:NetStatusEvent) {
		    if (stats.info.code == 'NetStream.Play.Stop') {
				dispatchEvent(new Event("VIDEO_COMPLETE"));
		    }
			if (stats.info.code == 'NetStream.Play.Start') {
				dispatchEvent(new Event("VIDEO_STARTED"));
			}
			trace(stats.info.code);
		}

		public function muteClicked(e:Event):void
		{
			switch(_initVolume) {
				case 0:
				    _initVolume = 1;
					break;
				case 1:
					_initVolume = 0;
					break;

			}

			sndTransform = new SoundTransform(_initVolume);
		    ns.soundTransform	= sndTransform;

		    if(isInit) {
		    	ns.seek(0);
				dispatchEvent(new Event("VIDEO_RESTARTED"));
		    	isInit = false;
		    }
		}
		
		//--------------------------------------
		//  PRIVATE & PROTECTED INSTANCE METHODS
		//--------------------------------------

	
	}

}


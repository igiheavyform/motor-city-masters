﻿package com.hf
{
	import flash.events.*;
	import flash.display.*;
	import flash.net.*;
	import flash.external.ExternalInterface;

	import com.greensock.*;
	import com.greensock.easing.*;
	import com.greensock.plugins.*;
	
	
	public class CloseBtn extends Sprite
	{
			
		//--------------------------------------
		// CLASS CONSTANTS
		//--------------------------------------

		private static const CLOSE_ALPHA:Number = .5;

		//--------------------------------------
		//  CONSTRUCTOR
		//--------------------------------------
	
		public function CloseBtn()
		{
			//TweenPlugin.activate([DropShadowFilterPlugin]);
			
			this.buttonMode = true;
			this.mouseChildren = false;
			
			this.addEventListener(MouseEvent.ROLL_OVER, bOver, false, 0, true);
			this.addEventListener(MouseEvent.ROLL_OUT, bOut, false, 0, true);
			this.addEventListener(MouseEvent.CLICK, bClick, false, 0, true);

			this.alpha = CLOSE_ALPHA;
		}

		//--------------------------------------
		//  PRIVATE VARIABLES
		//--------------------------------------
		
		private var click_url:String;
		private var dateNum:Number;
		private var linkOut:Boolean = true;

		//--------------------------------------
		//  GETTER/SETTERS
		//--------------------------------------
	
		//--------------------------------------
		//  PUBLIC METHODS
		//--------------------------------------
		
		//--------------------------------------
		//  EVENT HANDLERS
		//--------------------------------------
	
		private function bOver(e:MouseEvent):void
		{
			//TweenLite.to(watchTxt, .5, {glowFilter:{color:0xb67612, alpha:1, blurX:10, blurY:10, strength:5}});
		//	TweenLite.to(this, .3, {dropShadowFilter:{blurX:5, blurY:5, distance:0, alpha:1, strength:1, inner:true, color:0x000000}, ease:Quint.easeOut});
			
			//TweenLite.to(e.currentTarget.circleMC, .3, {tint:Constants.ORANGE_HEX, ease:Quint.easeOut});
			TweenLite.to(e.currentTarget, .5, {alpha:1, ease:Quint.easeOut});
			
			dispatchEvent(new Event("BUTTON_OVER"));
			//MovieClip(this.parent).expandItOver();
		}
		
		private function bOut(e:MouseEvent):void
		{
			//TweenLite.to(watchTxt, .5, {glowFilter:{color:0xb67612, alpha:1, blurX:10, blurY:10, strength:3}});
		//	TweenLite.to(this, .3, {dropShadowFilter:{blurX:5, blurY:5, distance:0, alpha:0, strength:1, inner:true, color:0x000000}, ease:Quint.easeOut});
			//TweenLite.to(e.currentTarget.circleMC, .3, {tint:null, ease:Quint.easeOut});
			TweenLite.to(e.currentTarget, .5, {alpha:CLOSE_ALPHA, ease:Quint.easeOut});
			
		//	TweenLite.to(arrowMC, .5, {glowFilter:{color:0xb67612, alpha:0, blurX:10, blurY:10, strength:1}});
		//	TweenLite.to(arrowMC.arrow, .5, {alpha:0, glowFilter:{color:0xffffff, alpha:0, blurX:10, blurY:10, strength:1}});
		}
		
		private function bClick(e:MouseEvent):void
		{
			this.removeEventListener(MouseEvent.ROLL_OVER, bOver);
			this.removeEventListener(MouseEvent.ROLL_OUT, bOut);
			this.removeEventListener(MouseEvent.CLICK, bClick);

			dispatchEvent(new Event("BUTTON_CLICKED"));
		}
	
		//--------------------------------------
		//  PRIVATE & PROTECTED INSTANCE METHODS
		//--------------------------------------
	
	}

}


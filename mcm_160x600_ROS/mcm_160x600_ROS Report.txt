﻿mcm_160x600_ROS.swf Movie Report
---------------------------------

Frame #    Frame Bytes    Total Bytes    Scene
-------    -----------    -----------    -----
      1          53344          53344    Scene 1 (AS 3.0 Classes Export Frame)

Scene      Shape Bytes    Text Bytes    ActionScript Bytes
-------    -----------    ----------    ------------------
Scene 1              0             0                 21540

Symbol         Shape Bytes    Text Bytes    ActionScript Bytes
-----------    -----------    ----------    ------------------
black_box               22             0                     0
btn                     25             0                     0
car image                0             0                     0
core                    33             0                     0
core copy                0             0                     0
core copy 2             33             0                     0
dateTxt                  0           507                     0
fb ico                  53             0                     0
flare                    0             0                     0
flare_BIG                0             0                     0
flare truTV              0             0                     0
glow                     0             0                     0
line                     0             0                     0
mask                     0             0                     0
MCM logo                 0             0                     0
MCMTT_mask               0             0                     0
sketch                   0             0                     0
streak                  20             0                     0
truTV                  164             0                     0
trutv_mask               0             0                     0
tune_up                  0           115                     0
tw ico                  78             0                     0
Txt                      0           299                     0
car mask                 0             0                     0
carm001                 30             0                     0
carm002                  0             0                     0
carm003                 36             0                     0
carm004                 70             0                     0
carm005                 48             0                     0
carm006                  0             0                     0
carm007                  0             0                     0
carm008                  0             0                     0
carm009                 46             0                     0
carm010                 48             0                     0
carm011                 68             0                     0
carm012                 83             0                     0
carm013                 68             0                     0
carmfill                 0             0                     0
mask001                  0             0                     0
mask002                  0             0                     0
mask003                 20             0                     0
mask004                  0             0                     0
mask005                  0             0                     0
mask006                  0             0                     0
mask007                  0             0                     0
mask008                  0             0                     0
mask009                  0             0                     0
mask010                  0             0                     0
mask011                  0             0                     0
mask012                  0             0                     0
mask013                  0             0                     0
mask014                  0             0                     0
mask015                  0             0                     0
mask016                  0             0                     0
mask019                  0             0                     0
mask020                  0             0                     0
mask021                  0             0                     0
mask023                  0             0                     0
mask024                 20             0                     0
mask025                 20             0                     0
mask026                  0             0                     0
mask027                  0             0                     0

Font Name                           Bytes         Characters
--------------------------------    ----------    ----------
DINCondensed-Bold Bold                    4952     .ABCDEFHIKLMOPRSTW 
HelveticaNeue-CondensedBold Bold          7398     /01249ACDEGHIJMNOPRSTUWY 
HelveticaNeue-Bold Bold                   6803     enptu 

ActionScript Bytes    Location
------------------    --------
             21540    Scene 1:Frame 1

Bitmap      Compressed    Original      Compression
--------    ----------    ----------    -----------
Bitmap 9          5743        439216    JPEG Quality=50
TT.png            2914         27232    JPEG Quality=60




